# practice01

单纯使用java代码统计 http://10.0.13.24/bigdata/res.txt.gz 该文件中，每个用户出现的次数和总的人数，内存使用不能超过1G

代码中要求使用 maven 包  commons-io 中的类 FileUtils 读写文件，具体使用方法自行百度

每个阶段 一个分支，比如第一阶段就是一个 版本，命名为 1.0 分支，以此迭代，分支开发完成后，需要合并到develop分支和master分支，并且全部推送到gitlab上